import logo from './logo.svg';
import './App.css';
import LoginButton from './components/login';
import LogoutButton from './components/logout';
import { useEffect } from 'react';
import { gapi } from 'gapi-script';

const clientId = '596868184554-f8hni6qi0bmacfrkri8aocmmni4u4heu.apps.googleusercontent.com';

function App() {

  useEffect(() => {
    function start() {
      gapi.client.init({
        clientId: clientId,
        scope: ""
      })
    };

    gapi.load('client:auth2', start);
  });

  return (
    <div className="App MyButton">
      <div className="MyButton">
      <LoginButton />
      </div>
      <div className="MyButton">
      <LogoutButton />
      </div>
    </div>
  );
}

export default App;
