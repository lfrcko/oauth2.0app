import { GoogleLogout } from 'react-google-login';
import { gapi } from 'gapi-script';

const clientId = '596868184554-f8hni6qi0bmacfrkri8aocmmni4u4heu.apps.googleusercontent.com';

function Logout() {
        
        const onSuccess = () => {
            alert('Logout made successfully');
        }
    
        return (
            <div id="signOutButton">
                <GoogleLogout
                    clientId={clientId}
                    buttonText={"Logout"}
                    onLogoutSuccess={onSuccess}
                />
            </div>
        )
    }

export default Logout;