import { GoogleLogin } from 'react-google-login';
import { gapi } from 'gapi-script';

const clientId = '596868184554-f8hni6qi0bmacfrkri8aocmmni4u4heu.apps.googleusercontent.com';

function Login() {
    
    const onSuccess = (res) => {
        console.log('[Login Success] currentUser:', res.profileObj);
        var accessToken = gapi.auth.getToken();
        console.log(accessToken);
    }
    
    const onFailure = (res) => {
        console.log('[Login Failed] res:', res);
    }

    return (
        <div id="signInButton">
            <GoogleLogin
                clientId={clientId}
                buttonText="Login"
                onSuccess={onSuccess}
                onFailure={onFailure}
                cookiePolicy={'single_host_origin'}
                isSignedIn={true}
            />
        </div>
    )
}

export default Login;